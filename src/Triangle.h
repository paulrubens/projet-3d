#ifndef TRIANGLE_H
#define TRIANGLE_H

#include "Vec3.h"
#include "Ray.h"
#include "Plane.h"
#include <cfloat>

#define LAMBDA2 0.0000001f

struct RayTriangleIntersection {
    bool intersectionExists;
    float t;
    float w0, w1, w2;
    unsigned int tIndex;
    Vec3 intersection;
    Vec3 normal;
};

/*
  struct MeshVertex {
        inline MeshVertex() {}

        inline MeshVertex(const Vec3 &_p, const Vec3 &_n) : position(_p), normal(_n), u(0), v(0) {}

        inline MeshVertex(const MeshVertex &vertex) : position(vertex.position), normal(vertex.normal), u(vertex.u),
                                                      v(vertex.v) {}

        inline virtual ~MeshVertex() {}

        inline MeshVertex &operator=(const MeshVertex &vertex) {
            position = vertex.position;
            normal = vertex.normal;
            u = vertex.u;
            v = vertex.v;
            return (*this);
        }

        // membres :
        Vec3 position; // une position
        Vec3 normal; // une normale
        float u, v; // coordonnees uv
    };

    struct MeshTriangle {
        inline MeshTriangle() {
            v[0] = v[1] = v[2] = 0;
        }

        inline MeshTriangle(const MeshTriangle &t) {
            v[0] = t.v[0];
            v[1] = t.v[1];
            v[2] = t.v[2];
        }

        inline MeshTriangle(unsigned int v0, unsigned int v1, unsigned int v2) {
            v[0] = v0;
            v[1] = v1;
            v[2] = v2;
        }

        unsigned int &operator[](unsigned int iv) { return v[iv]; }

        unsigned int operator[](unsigned int iv) const { return v[iv]; }

        inline virtual ~MeshTriangle() {}

        inline MeshTriangle &operator=(const MeshTriangle &t) {
            v[0] = t.v[0];
            v[1] = t.v[1];
            v[2] = t.v[2];
            return (*this);
        }

        // membres :
        unsigned int v[3];
    };

  RayTriangleIntersection intersect(const Ray &ray) const {
        RayTriangleIntersection closestIntersection;
        closestIntersection.t = FLT_MAX;
        for (int i = 0; i < this->triangles.size(); i++) {
            Triangle T(this->vertices[this->triangles[i].v[0]].position, //modifier pour être position dans la grille
                       this->vertices[this->triangles[i].v[1]].position,
                       this->vertices[this->triangles[i].v[2]].position);
            RayTriangleIntersection RTI = T.getIntersection(ray);
            if (RTI.intersectionExists && RTI.t < closestIntersection.t) {
                closestIntersection = RTI;
                closestIntersection.tIndex = i;
            }
        }
        return closestIntersection;
    }
 */

#endif