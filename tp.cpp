#include <functional>
#include <unordered_set>
#include <iostream>
#include <fstream>
#include <numeric>
#include <vector>
#include <algorithm>
#include <string>
#include <cstdio>
#include <cstdlib>
#include <GL/glut.h>
#include <float.h>
#include "src/Vec3.h"
#include "src/Camera.h"
#include "src/Ray.h"
#include "src/Triangle.h"

//grille
std::vector <Vec3> positionsGrille;
std::vector <Vec3> normalsGrille;

//voxelisation du mesh
std::vector<Vec3> voxelCenters;

//résolution
int gridSize = 32;

//step
const float minVal = -1.0f;
const float maxVal = 1.0f;
Vec3 gridMin = {minVal, minVal, minVal};
Vec3 gridMax = {maxVal, maxVal, maxVal};
const float step = (maxVal - minVal) / (gridSize - 1);  //interval entre les points

//ne calculer qu'une seule fois les voxels à placer
bool drawOnce = true;

// les résultats du RMSE
double rmseApresSimplification = -1.0;
double rmseApresFermeture = -1.0;
double rmseApresConnect = -1.0;

enum DisplayMode {
    WIRE = 0, SOLID = 1, LIGHTED_WIRE = 2, LIGHTED = 3
};

void drawText(const std::string& text, float x, float y) {
    glColor3f(1.0f, 1.0f, 1.0f); //texte en blanc pour visibilité
    glRasterPos2f(x, y);
    for (char c : text) {
        glutBitmapCharacter(GLUT_BITMAP_8_BY_13, c);
    }
}

struct Button {
    Vec3 position; // Position du centre du bouton
    float width;   // Largeur du bouton
    float height;  // Hauteur du bouton
    std::string label; // Étiquette du bouton
    std::function<void()> onClick; // Fonction à exécuter lors du clic sur le bouton
};

std::vector<Button> buttons;

struct Triangle {

    Vec3 m_c[3], m_normal; // Coordonnées et normal du triangle
    float area; // Aire du triangle
    unsigned int v[3]; // Indices des sommets du triangle

    // Constructeurs
    Triangle() : m_c{Vec3(), Vec3(), Vec3()}, m_normal(Vec3()), area(0), v{0, 0, 0} {}

    Triangle(unsigned int v0, unsigned int v1, unsigned int v2)
            : v{v0, v1, v2}, m_c{Vec3(), Vec3(), Vec3()}, m_normal(Vec3()), area(0) {}

    Triangle(Vec3 const &c0, Vec3 const &c1, Vec3 const &c2) : m_c{c0, c1, c2}, v{0, 0, 0} {
        updateAreaAndNormal();
    }

    void updateAreaAndNormal() {
        Vec3 nNotNormalized = Vec3::cross(m_c[1] - m_c[0], m_c[2] - m_c[0]);
        float norm = nNotNormalized.length();
        m_normal = nNotNormalized / norm;
        area = norm / 2.f;
    }

    void setC0(Vec3 const &c0) { m_c[0] = c0; }
    void setC1(Vec3 const &c1) { m_c[1] = c1; }
    void setC2(Vec3 const &c2) { m_c[2] = c2; }
    Vec3 const &normal() const { return m_normal; }

    unsigned int &operator[](unsigned int iv) { return v[iv]; }

    unsigned int operator[](unsigned int iv) const { return v[iv]; }

    inline virtual ~Triangle() {}

    inline Triangle &operator=(const Triangle &t) {
        v[0] = t.v[0];
        v[1] = t.v[1];
        v[2] = t.v[2];
        return (*this);
    }

    void computeBarycentricCoordinates(Vec3 const &p, float &u0, float &u1, float &u2) const {
        Vec3 BA = (this->m_c[1] - this->m_c[0]);
        Vec3 BC = (this->m_c[2] - this->m_c[1]);
        Vec3 CA = (this->m_c[0] - this->m_c[2]);

        Vec3 p0 = Vec3::cross(BA, (p - this->m_c[0]));
        Vec3 p1 = Vec3::cross(BC, (p - this->m_c[1]));
        Vec3 p2 = Vec3::cross(CA, (p - this->m_c[2]));

        float area0 = p0.length() / 2.f;
        float area1 = p1.length() / 2.f;
        float area2 = p2.length() / 2.f;

        u0 = area0 / Vec3::cross(BA, CA).length();
        u1 = area1 / Vec3::cross(BA, CA).length();
        u2 = area2 / Vec3::cross(BA, CA).length();
    }

    //méthode wikipédia (Möller–Trumbore intersection algorithm)
    RayTriangleIntersection getIntersection(Ray const &ray) const {
        RayTriangleIntersection result;

        Vec3 v0 = this->m_c[0];
        Vec3 v1 = this->m_c[1];
        Vec3 v2 = this->m_c[2];
        Vec3 cote1 = v1 - v0;
        Vec3 cote2 = v2 - v0;
        Vec3 h = Vec3::cross(ray.direction(), cote2);
        float a = Vec3::dot(cote1, h);

        if (a < LAMBDA2) { //rayon parallèle
            result.intersectionExists = false;
            result.t = FLT_MAX; // mise à max pour être sûr d'éviter une erreur
            return result;
        }

        float f = 1 / a;
        Vec3 s = ray.origin() - v0;
        float u = f * Vec3::dot(s, h);
        if (u < 0 || u > 1) {
            result.intersectionExists = false;
            result.t = FLT_MAX; // mise à max pour être sûr d'éviter une erreur
            return result;
        }

        Vec3 q = Vec3::cross(s, cote1);
        float v = f * Vec3::dot(ray.direction(), q);
        if (v < 0 || u + v > 1) {
            result.intersectionExists = false;
            result.t = FLT_MAX; // mise à max pour être sûr d'éviter une erreur
            return result;
        }

        //on a une intersection
        float t = f * Vec3::dot(cote2, q);
        Vec3 P = ray.origin() + t * ray.direction();
        float w0, w1, w2;
        computeBarycentricCoordinates(P, w0, w1, w2);
        result.intersectionExists = true;
        result.t = t;
        result.w0 = w0;
        result.w1 = w1;
        result.w2 = w2;
        result.tIndex = 0;
        result.intersection = P;
        result.normal = this->m_normal;

        return result;
    }
};

struct Edge {
    unsigned int debut;
    unsigned int fin;

    Edge(unsigned int _debut, unsigned int _fin) : debut(_debut), fin(_fin) {}

    bool operator==(const Edge& other) const {
        return (debut == other.debut && fin == other.fin) || (debut == other.fin && fin == other.debut);
    }

    bool estDansTriangle(const Triangle& triangle) const {
        bool debutTrouve = false, finTrouve = false;
        for (int i = 0; i < 3; ++i) {
            if (triangle[i] == debut) debutTrouve = true;
            if (triangle[i] == fin) finTrouve = true;
        }
        return debutTrouve && finTrouve;
    }

    bool sommetDansTriangle(const Triangle &triangle, const unsigned int &sommet) const{
        bool trouve = false;
        for (int i = 0; i < 3; ++i) {
            if (triangle[i] == sommet) trouve = true;
        }
        return trouve;
    }
};

struct Mesh {
    std::vector <Vec3> vertices; //array of mesh vertices positions
    std::vector <Vec3> normals; //array of vertices normals useful for the display
    std::vector <Triangle> triangles; //array of mesh triangles
    std::vector<Edge> edges;
    std::vector <Vec3> triangle_normals; //triangle normals to display face normals
    std::vector<std::vector<unsigned int> > oneRing;

    void computeTrianglesNormals() {
        triangle_normals.clear();
        for (unsigned int i = 0; i < triangles.size(); i++) {
            const Vec3 &e0 = vertices[triangles[i][1]] - vertices[triangles[i][0]];
            const Vec3 &e1 = vertices[triangles[i][2]] - vertices[triangles[i][0]];
            Vec3 n = Vec3::cross(e0, e1);
            n.normalize();
            triangle_normals.push_back(n);
        }
    }

    void computeVerticesNormals() {
        normals.clear();
        normals.resize(vertices.size(), Vec3(0., 0., 0.));

        for (unsigned int i = 0; i < triangles.size(); i++) {
            for (unsigned int t = 0; t < 3; t++) {
                normals[triangles[i][t]] += triangle_normals[i];
            }
        }

        for (unsigned int i = 0; i < vertices.size(); i++) {
            normals[i].normalize();
        }
    }

    void computeNormals() {
        computeTrianglesNormals();
        computeVerticesNormals();
    }


    void ajouterEdgeSiUnique(const Edge& edge) {
        for (const Edge& e : edges) {
            if (e == edge) return;
        }
        edges.push_back(edge);
    }

    void addNoise(){
        for( unsigned int i = 0 ; i < vertices.size() ; i ++ ){
            float factor = 0.03;
            const Vec3 & p = vertices[i];
            const Vec3 & n = normals[i];
            vertices[i] = Vec3( p[0] + factor*((double)(rand()) / (double)
                    (RAND_MAX))*n[0], p[1] + factor*((double)(rand()) / (double)
                    (RAND_MAX))*n[1], p[2] + factor*((double)(rand()) / (double)
                    (RAND_MAX))*n[2]);
        }
    }

};

//Transformation made of a rotation and translation
struct Transformation {
    Mat3 rotation;
    Vec3 translation;
};


bool contain(std::vector < unsigned int >
const & i_vector,
unsigned int element
) {
for (
unsigned int i = 0;
i<i_vector.

size();

i++) {
if (i_vector[i] == element) return true;
}
return false;
}

void collect_one_ring(std::vector < Vec3 >
const & i_vertices,
std::vector <Triangle> const &i_triangles,
        std::vector<std::vector < unsigned int>
> & o_one_ring) {
o_one_ring.

clear();

o_one_ring.
resize(i_vertices
.

size()

); //one-ring of each vertex, i.e. a list of vertices with which it shares an edge
//Parcourir les triangles et ajouter les voisins dans le 1-voisinage
//Attention verifier que l'indice n'est pas deja present
for (
unsigned int i = 0;
i<i_triangles.

size();

i++) {
//Tous les points opposés dans le triangle sont reliés
for (
int j = 0;
j < 3; j++) {
for (
int k = 0;
k < 3; k++) {
if (j != k) {
if (!
contain(o_one_ring[i_triangles[i][j]], i_triangles[i][k]
)) {
o_one_ring[i_triangles[i][j]].
push_back(i_triangles[i][k]);
}
}
}
}
}
}


//Input mesh loaded at the launch of the application
Mesh mesh;
std::vector<float> current_field; //normalized filed of each vertex

bool display_normals;
bool display_smooth_normals;
bool display_mesh;

DisplayMode displayMode;
int weight_type;

// -------------------------------------------
// OpenGL/GLUT application code.
// -------------------------------------------

static GLint window;
static unsigned int SCREENWIDTH = 1600;
static unsigned int SCREENHEIGHT = 900;
static Camera camera;
static bool mouseRotatePressed = false;
static bool mouseMovePressed = false;
static bool mouseZoomPressed = false;
static int lastX = 0, lastY = 0, lastZoom = 0;
static bool fullScreen = false;

// ------------------------------------
// File I/O
// ------------------------------------
bool saveOFF(const std::string &filename,
             std::vector <Vec3> const &i_vertices,
             std::vector <Vec3> const &i_normals,
             std::vector <Triangle> const &i_triangles,
             std::vector <Vec3> const &i_triangle_normals,
             bool save_normals = false) {
    std::ofstream myfile;
    myfile.open(filename.c_str());
    if (!myfile.is_open()) {
        std::cout << filename << " cannot be opened" << std::endl;
        return false;
    }

    myfile << "OFF" << std::endl;

    unsigned int n_vertices = i_vertices.size(), n_triangles = i_triangles.size();
    myfile << n_vertices << " " << n_triangles << " 0" << std::endl;

    for (unsigned int v = 0; v < n_vertices; ++v) {
        myfile << i_vertices[v][0] << " " << i_vertices[v][1] << " " << i_vertices[v][2] << " ";
        if (save_normals) myfile << i_normals[v][0] << " " << i_normals[v][1] << " " << i_normals[v][2] << std::endl;
        else myfile << std::endl;
    }
    for (unsigned int f = 0; f < n_triangles; ++f) {
        myfile << 3 << " " << i_triangles[f][0] << " " << i_triangles[f][1] << " " << i_triangles[f][2] << " ";
        if (save_normals)
            myfile << i_triangle_normals[f][0] << " " << i_triangle_normals[f][1] << " " << i_triangle_normals[f][2];
        myfile << std::endl;
    }
    myfile.close();
    return true;
}

void openOFF(std::string const &filename,
             std::vector <Vec3> &o_vertices,
             std::vector <Vec3> &o_normals,
             std::vector <Triangle> &o_triangles,
             std::vector <Vec3> &o_triangle_normals,
             bool load_normals = true) {
    std::ifstream myfile;
    myfile.open(filename.c_str());
    if (!myfile.is_open()) {
        std::cout << filename << " cannot be opened" << std::endl;
        return;
    }

    std::string magic_s;

    myfile >> magic_s;

    if (magic_s != "OFF") {
        std::cout << magic_s << " != OFF :   We handle ONLY *.off files." << std::endl;
        myfile.close();
        exit(1);
    }

    int n_vertices, n_faces, dummy_int;
    myfile >> n_vertices >> n_faces >> dummy_int;

    o_vertices.clear();
    o_normals.clear();

    for (int v = 0; v < n_vertices; ++v) {
        float x, y, z;

        myfile >> x >> y >> z;
        o_vertices.push_back(Vec3(x, y, z));

        if (load_normals) {
            myfile >> x >> y >> z;
            o_normals.push_back(Vec3(x, y, z));
        }
    }

    o_triangles.clear();
    o_triangle_normals.clear();
    for (int f = 0; f < n_faces; ++f) {
        int n_vertices_on_face;
        myfile >> n_vertices_on_face;

        if (n_vertices_on_face == 3) {
            unsigned int _v1, _v2, _v3;
            myfile >> _v1 >> _v2 >> _v3;

            o_triangles.push_back(Triangle(_v1, _v2, _v3));

            mesh.ajouterEdgeSiUnique(Edge(_v1, _v2));
            mesh.ajouterEdgeSiUnique(Edge(_v1, _v3));
            mesh.ajouterEdgeSiUnique(Edge(_v2, _v3));

            if (load_normals) {
                float x, y, z;
                myfile >> x >> y >> z;
                o_triangle_normals.push_back(Vec3(x, y, z));
            }
        } else if (n_vertices_on_face == 4) {
            unsigned int _v1, _v2, _v3, _v4;
            myfile >> _v1 >> _v2 >> _v3 >> _v4;

            o_triangles.push_back(Triangle(_v1, _v2, _v3));
            o_triangles.push_back(Triangle(_v1, _v3, _v4));

            mesh.ajouterEdgeSiUnique(Edge(_v1, _v2));
            mesh.ajouterEdgeSiUnique(Edge(_v1, _v3));
            mesh.ajouterEdgeSiUnique(Edge(_v2, _v3));
            mesh.ajouterEdgeSiUnique(Edge(_v1, _v4));
            mesh.ajouterEdgeSiUnique(Edge(_v4, _v3));

            if (load_normals) {
                float x, y, z;
                myfile >> x >> y >> z;
                o_triangle_normals.push_back(Vec3(x, y, z));
            }

        } else {
            std::cout << "We handle ONLY *.off files with 3 or 4 vertices per face" << std::endl;
            myfile.close();
            exit(1);
        }

    }

}

// ------------------------------------
// Application initialization
// ------------------------------------
void initLight() {
    GLfloat light_position1[4] = {22.0f, 16.0f, 50.0f, 0.0f};
    GLfloat direction1[3] = {-52.0f, -16.0f, -50.0f};
    GLfloat color1[4] = {1.0f, 1.0f, 1.0f, 1.0f};
    GLfloat ambient[4] = {0.3f, 0.3f, 0.3f, 0.5f};

    glLightfv(GL_LIGHT1, GL_POSITION, light_position1);
    glLightfv(GL_LIGHT1, GL_SPOT_DIRECTION, direction1);
    glLightfv(GL_LIGHT1, GL_DIFFUSE, color1);
    glLightfv(GL_LIGHT1, GL_SPECULAR, color1);
    glLightModelfv(GL_LIGHT_MODEL_AMBIENT, ambient);
    glEnable(GL_LIGHT1);
    glEnable(GL_LIGHTING);
}

void init() {
    camera.resize(SCREENWIDTH, SCREENHEIGHT);
    initLight();
    glCullFace(GL_BACK);
    glDisable(GL_CULL_FACE);
    glDepthFunc(GL_LESS);
    glEnable(GL_DEPTH_TEST);
    glClearColor(0.2f, 0.2f, 0.3f, 1.0f);
    glEnable(GL_COLOR_MATERIAL);
    glLightModeli(GL_LIGHT_MODEL_TWO_SIDE, GL_TRUE);

    display_normals = false;
    display_mesh = true;
    display_smooth_normals = true;
    displayMode = LIGHTED;

}

// ------------------------------------
// Rendering.
// ------------------------------------

void drawVector(Vec3 const &i_from, Vec3 const &i_to) {

    glBegin(GL_LINES);
    glVertex3f(i_from[0], i_from[1], i_from[2]);
    glVertex3f(i_to[0], i_to[1], i_to[2]);
    glEnd();
}

void drawAxis(Vec3 const &i_origin, Vec3 const &i_direction) {

    glLineWidth(4); // for example...
    drawVector(i_origin, i_origin + i_direction);
}

void drawReferenceFrame(Vec3 const &origin, Vec3 const &i, Vec3 const &j, Vec3 const &k) {

    glDisable(GL_LIGHTING);
    glColor3f(0.8, 0.2, 0.2);
    drawAxis(origin, i);
    glColor3f(0.2, 0.8, 0.2);
    drawAxis(origin, j);
    glColor3f(0.2, 0.2, 0.8);
    drawAxis(origin, k);
    glEnable(GL_LIGHTING);

}


/**
 * Calculs Simplification
 */

float longueurEdge(Vec3 v1, Vec3 v2) {
    return sqrt((v1[0] - v2[0]) * (v1[0] - v2[0]) + (v1[1] - v2[1]) * (v1[1] - v2[1]) + (v1[2] - v2[2]) * (v1[2] - v2[2]));
}

void supprimerEdge(Mesh& mesh, Edge edge) {
    mesh.edges.erase(std::remove(mesh.edges.begin(), mesh.edges.end(), edge), mesh.edges.end());
}

void supprimerEdgesAssociesAuSommet(Mesh& mesh, unsigned int sommet) {
    mesh.edges.erase(std::remove_if(mesh.edges.begin(), mesh.edges.end(),
                                    [sommet](const Edge& e) { return e.debut == sommet || e.fin == sommet; }), mesh.edges.end());
}

void updateTrianglesForSplitEdge(unsigned int oldV1, unsigned int oldV2, unsigned int newVertexIndex, Mesh& mesh) {
    std::vector <Triangle> newTriangles;
    for (const Triangle &tri: mesh.triangles) {
        // Vérifier si le triangle actuel contient les sommets oldV1 et oldV2 (l'arête à fractionner).
        if ((tri[0] == oldV1 && tri[1] == oldV2) || (tri[1] == oldV1 && tri[0] == oldV2)) {
            // si entre 0 et 1, créer deux nouveaux triangles
            newTriangles.emplace_back(oldV1, newVertexIndex, tri[2]);
            newTriangles.emplace_back(newVertexIndex, oldV2, tri[2]);
        } else if ((tri[1] == oldV1 && tri[2] == oldV2) || (tri[2] == oldV1 && tri[1] == oldV2)) {
            // si entre 1 et 2, créer deux nouveaux triangles
            newTriangles.emplace_back(tri[0], oldV1, newVertexIndex);
            newTriangles.emplace_back(tri[0], newVertexIndex, oldV2);
        } else if ((tri[0] == oldV1 && tri[2] == oldV2) || (tri[2] == oldV1 && tri[0] == oldV2)) {
            // si entre 0 et 2, créer deux nouveaux triangles
            newTriangles.emplace_back(oldV1, tri[1], newVertexIndex);
            newTriangles.emplace_back(newVertexIndex, tri[1], oldV2);
        } else {
            // triangle originel
            newTriangles.push_back(tri);
        }
    }
    //à refaire?
    //ne pas recréer la liste des triangles à chaque fois
    mesh.triangles = newTriangles;
    mesh.computeNormals();
}

void splitEdge(Edge e, Mesh& mesh) {
    unsigned int v1 = e.debut;
    unsigned int v2 = e.fin;
    Vec3 midPoint = Vec3(
            (mesh.vertices[v1][0] + mesh.vertices[v2][0]) * 0.5,
            (mesh.vertices[v1][1] + mesh.vertices[v2][1]) * 0.5,
            (mesh.vertices[v1][2] + mesh.vertices[v2][2]) * 0.5
    );
    unsigned int newVertexIndex = mesh.vertices.size();
    mesh.vertices.push_back(midPoint);

    //mettre à jour triangles
    updateTrianglesForSplitEdge(v1, v2, newVertexIndex, mesh);
    mesh.ajouterEdgeSiUnique(Edge(v1, newVertexIndex));
    mesh.ajouterEdgeSiUnique(Edge(v2, newVertexIndex));
    supprimerEdge(mesh, e);

    mesh.computeNormals();
}

bool isNormalFlipped(const Vec3& oldNormal, const Vec3& newNormal) {
    return Vec3::dot(oldNormal, newNormal) < 0;
}

bool updateTrianglesForCollapseEdge(unsigned int v1, unsigned int v2, Mesh& mesh) {
    std::vector<Triangle> newTriangles;
    for (const Triangle& tri : mesh.triangles) {
        if (tri[0] == v2 || tri[1] == v2 || tri[2] == v2) {
            Triangle updatedTriangle = tri;
            for (int i = 0; i < 3; ++i) {
                if (updatedTriangle[i] == v2) {
                    updatedTriangle[i] = v1;
                }
            }

            Vec3 oldNormal = mesh.triangle_normals[&tri - &mesh.triangles[0]];
            Vec3 newNormal = Vec3::cross(mesh.vertices[updatedTriangle[1]] - mesh.vertices[updatedTriangle[0]],
                                         mesh.vertices[updatedTriangle[2]] - mesh.vertices[updatedTriangle[0]]);
            newNormal.normalize();

            if (!isNormalFlipped(oldNormal, newNormal)) {
                newTriangles.push_back(updatedTriangle);
            } else {
                return false; //cancel collapse
            }
        } else {
            newTriangles.push_back(tri);
        }
    }

    mesh.triangles = newTriangles;
    mesh.computeNormals();
    return true; // Collapse
}

void collapseEdge(Edge e, Mesh& mesh) {
    unsigned int v1 = e.debut;
    unsigned int v2 = e.fin;
    Vec3 midPoint = (mesh.vertices[v1] + mesh.vertices[v2]) * 0.5;
    Vec3 v1Copy = mesh.vertices[v1];
    mesh.vertices[v1] = midPoint;

    if (!updateTrianglesForCollapseEdge(v1, v2, mesh)) {
        // invalid collapse
        mesh.vertices[v1] = v1Copy;
    } else {
        supprimerEdgesAssociesAuSommet(mesh, v2);
    }
}

void flipEdge(Edge e, Mesh& mesh) {
    std::vector<unsigned int> adjacentTriangles;

    for (unsigned int i = 0; i < mesh.triangles.size(); ++i) {
        const Triangle& tri = mesh.triangles[i];
        if (e.estDansTriangle(tri)) {
            adjacentTriangles.push_back(i);
        }
    }

    if (adjacentTriangles.size() != 2) {
        return;
    }

    unsigned int unsharedVertices[2];
    int count = 0;
    for (int i = 0; i < 3; ++i) {
        if (mesh.triangles[adjacentTriangles[0]][i] != e.debut && mesh.triangles[adjacentTriangles[0]][i] != e.fin) {
            unsharedVertices[0] = mesh.triangles[adjacentTriangles[0]][i];
        }
        if (mesh.triangles[adjacentTriangles[1]][i] != e.debut && mesh.triangles[adjacentTriangles[1]][i] != e.fin) {
            unsharedVertices[1] = mesh.triangles[adjacentTriangles[1]][i];
        }
    }

    Triangle newTri1(unsharedVertices[0], e.debut, unsharedVertices[1]);
    Triangle newTri2(unsharedVertices[0], unsharedVertices[1], e.fin);

    mesh.triangles[adjacentTriangles[0]] = newTri1;
    mesh.triangles[adjacentTriangles[1]] = newTri2;

    mesh.computeNormals();
}

/*
 * parcours des triangles et supressions des triangles avec des sommets confondus
 */
void elimineTADCs(Mesh& mesh) {
    std::cout << "Début suppression: " << mesh.triangles.size() << std::endl;
    for(int t = mesh.triangles.size()-1; t >= 0; --t){
        int s0 = mesh.triangles[t][0];
        int s1 = mesh.triangles[t][1];
        int s2 = mesh.triangles[t][2];
        //std::cout << s0 << " " << s1 << " " << s2 << std::endl;

        if(s0 == s1 || s1 == s2 || s2 == s0){
            supprimerEdgesAssociesAuSommet(mesh, s0);
            supprimerEdgesAssociesAuSommet(mesh, s1);
            supprimerEdgesAssociesAuSommet(mesh, s2);
            mesh.triangles.erase(mesh.triangles.begin() + t);
        }
    }
    std::cout << "Fin suppression: " << mesh.triangles.size() << std::endl;
    mesh.computeNormals();
}

void applySmoothing(float factor) {
    std::vector<Vec3> nouvSommets(mesh.vertices.size(), Vec3(0, 0, 0));
    std::vector<int> neighbor_count(mesh.vertices.size(), 0);

    for (const Triangle &tri: mesh.triangles) {
        for (int k = 0; k < 3; k++) {
            Vec3 p0 = mesh.vertices[tri[k]];
            Vec3 p1 = mesh.vertices[tri[(k + 1) % 3]];
            Vec3 p2 = mesh.vertices[tri[(k + 2) % 3]];

            nouvSommets[tri[k]] += p1 + p2;
            neighbor_count[tri[k]] += 2; // p1 et p2 sont voisins de p0
        }
    }

    for (unsigned int j = 0; j < mesh.vertices.size(); j++) {
        nouvSommets[j] = (nouvSommets[j] / neighbor_count[j]) - mesh.vertices[j];
        nouvSommets[j] = mesh.vertices[j] + factor * nouvSommets[j]; // appliquer le lissage
    }

    mesh.vertices = nouvSommets;
}

void taubinSmooth(float lambda, float mu, unsigned int _iters) {
    for (int i = 0; i < _iters; i++) {
        applySmoothing(lambda);
        applySmoothing(mu);

        mesh.computeNormals();
        std::cout << "\rItération n°" << i + 1 << std::flush;
    }
    std::cout << std::endl;
}

//fin calculs simplification

typedef struct {
    float r;       // ∈ [0, 1]
    float g;       // ∈ [0, 1]
    float b;       // ∈ [0, 1]
} RGB;


RGB scalarToRGB(float scalar_value) //Scalar_value ∈ [0, 1]
{
    RGB rgb;
    float H = scalar_value * 360., S = 1., V = 0.85,
            P, Q, T,
            fract;

    (H == 360.) ? (H = 0.) : (H /= 60.);
    fract = H - floor(H);

    P = V * (1. - S);
    Q = V * (1. - S * fract);
    T = V * (1. - S * (1. - fract));

    if (0. <= H && H < 1.)
        rgb = (RGB) {.r = V, .g = T, .b = P};
    else if (1. <= H && H < 2.)
        rgb = (RGB) {.r = Q, .g = V, .b = P};
    else if (2. <= H && H < 3.)
        rgb = (RGB) {.r = P, .g = V, .b = T};
    else if (3. <= H && H < 4.)
        rgb = (RGB) {.r = P, .g = Q, .b = V};
    else if (4. <= H && H < 5.)
        rgb = (RGB) {.r = T, .g = P, .b = V};
    else if (5. <= H && H < 6.)
        rgb = (RGB) {.r = V, .g = P, .b = Q};
    else
        rgb = (RGB) {.r = 0., .g = 0., .b = 0.};

    return rgb;
}

void drawSmoothTriangleMesh(Mesh const &i_mesh, bool draw_field = false) {
    glBegin(GL_TRIANGLES);
    for (unsigned int tIt = 0; tIt < i_mesh.triangles.size(); ++tIt) {

        for (unsigned int i = 0; i < 3; i++) {
            const Vec3 &p = i_mesh.vertices[i_mesh.triangles[tIt][i]]; //Vertex position
            const Vec3 &n = i_mesh.normals[i_mesh.triangles[tIt][i]]; //Vertex normal

            if (draw_field && current_field.size() > 0) {
                RGB color = scalarToRGB(current_field[i_mesh.triangles[tIt][i]]);
                glColor3f(color.r, color.g, color.b);
            }
            glNormal3f(n[0], n[1], n[2]);
            glVertex3f(p[0], p[1], p[2]);
        }
    }
    glEnd();

}

void drawTriangleMesh(Mesh const &i_mesh, bool draw_field = false) {
    glBegin(GL_TRIANGLES);
    for (unsigned int tIt = 0; tIt < i_mesh.triangles.size(); ++tIt) {
        const Vec3 &n = i_mesh.triangle_normals[tIt]; //Triangle normal
        for (unsigned int i = 0; i < 3; i++) {
            const Vec3 &p = i_mesh.vertices[i_mesh.triangles[tIt][i]]; //Vertex position

            if (draw_field) {
                RGB color = scalarToRGB(current_field[i_mesh.triangles[tIt][i]]);
                glColor3f(color.r, color.g, color.b);
            }
            glNormal3f(n[0], n[1], n[2]);
            glVertex3f(p[0], p[1], p[2]);
        }
    }
    glEnd();

}

void drawMesh(Mesh const &i_mesh, bool draw_field = false) {
    if (display_smooth_normals)
        drawSmoothTriangleMesh(i_mesh, draw_field); //Smooth display with vertices normals
    else
        drawTriangleMesh(i_mesh, draw_field); //Display with face normals
}

void drawVectorField(std::vector < Vec3 > const & i_positions, std::vector <Vec3> const &i_directions) {
glLineWidth(1.);
for(unsigned int pIt = 0; pIt < i_directions.size(); ++pIt) {
Vec3 to = i_positions[pIt] + 0.02 * i_directions[pIt];
drawVector(i_positions[pIt], to);
}
}

void drawNormals(Mesh const &i_mesh) {

    if (display_smooth_normals) {
        drawVectorField(i_mesh.vertices, i_mesh.normals);
    } else {
        std::vector <Vec3> triangle_baricenters;
        for (const Triangle &triangle: i_mesh.triangles) {
            Vec3 triangle_baricenter(0., 0., 0.);
            for (unsigned int i = 0; i < 3; i++)
                triangle_baricenter += i_mesh.vertices[triangle[i]];
            triangle_baricenter /= 3.;
            triangle_baricenters.push_back(triangle_baricenter);
        }

        drawVectorField(triangle_baricenters, i_mesh.triangle_normals);
    }
}


void drawPointSet(std::vector < Vec3 > const & i_positions , std::vector <Vec3> const &i_normals){
glBegin(GL_POINTS);
for(unsigned int pIt = 0; pIt < i_positions.size(); ++pIt) {
glNormal3f( i_normals[pIt][0], i_normals[pIt][1], i_normals[pIt][2]);
glVertex3f( i_positions[pIt][0], i_positions[pIt][1], i_positions[pIt][2]);
}
glEnd();
}

void drawButton(const Button& button) {
    Vec3 topLeft = button.position - Vec3(button.width / 2, button.height / 2, 0);
    Vec3 bottomRight = button.position + Vec3(button.width / 2, button.height / 2, 0);

    glColor3f(0.1f, 0.1f, 0.8f); // Couleur du bouton
    glBegin(GL_QUADS);
    glVertex3f(topLeft[0], topLeft[1], 0);
    glVertex3f(bottomRight[0], topLeft[1], 0);
    glVertex3f(bottomRight[0], bottomRight[1], 0);
    glVertex3f(topLeft[0], bottomRight[1], 0);
    glEnd();

    drawText(button.label, button.position[0]+50, button.position[1]-5);
}

//création d'un voxel
void DisplayVoxel(Vec3 centre, double length, Vec3 color){
    glBegin(GL_QUADS);
    glPolygonMode(GL_FRONT, GL_FILL);
    //glColor3f(0.2f,0.1f,0.6f);
    glColor3f(color[0],color[1],color[2]);

    Vec3 halfLength = Vec3(length / 2, length / 2, length / 2);
    Vec3 upLeftFront = centre + Vec3(-halfLength[0], halfLength[1], halfLength[2]);
    Vec3 upRightFront = centre + Vec3(halfLength[0], halfLength[1], halfLength[2]);
    Vec3 bottomLeftFront = centre + Vec3(-halfLength[0], -halfLength[1], halfLength[2]);
    Vec3 bottomRightFront = centre + Vec3(halfLength[0], -halfLength[1], halfLength[2]);
    Vec3 upLeftBack = centre + Vec3(-halfLength[0], halfLength[1], -halfLength[2]);
    Vec3 upRightBack = centre + Vec3(halfLength[0], halfLength[1], -halfLength[2]);
    Vec3 bottomLeftBack = centre + Vec3(-halfLength[0], -halfLength[1], -halfLength[2]);
    Vec3 bottomRightBack = centre + Vec3(halfLength[0], -halfLength[1], -halfLength[2]);

    // Face avant
    glVertex3f(upLeftFront[0], upLeftFront[1], upLeftFront[2]);
    glVertex3f(bottomLeftFront[0], bottomLeftFront[1], bottomLeftFront[2]);
    glVertex3f(bottomRightFront[0], bottomRightFront[1], bottomRightFront[2]);
    glVertex3f(upRightFront[0], upRightFront[1], upRightFront[2]);

    // Face arrière
    glVertex3f(upRightBack[0], upRightBack[1], upRightBack[2]);
    glVertex3f(bottomRightBack[0], bottomRightBack[1], bottomRightBack[2]);
    glVertex3f(bottomLeftBack[0], bottomLeftBack[1], bottomLeftBack[2]);
    glVertex3f(upLeftBack[0], upLeftBack[1], upLeftBack[2]);

    // Mur gauche
    glVertex3f(upLeftBack[0], upLeftBack[1], upLeftBack[2]);
    glVertex3f(bottomLeftBack[0], bottomLeftBack[1], bottomLeftBack[2]);
    glVertex3f(bottomLeftFront[0], bottomLeftFront[1], bottomLeftFront[2]);
    glVertex3f(upLeftFront[0], upLeftFront[1], upLeftFront[2]);

    // Mur droite
    glVertex3f(upRightFront[0], upRightFront[1], upRightFront[2]);
    glVertex3f(bottomRightFront[0], bottomRightFront[1], bottomRightFront[2]);
    glVertex3f(bottomRightBack[0], bottomRightBack[1], bottomRightBack[2]);
    glVertex3f(upRightBack[0], upRightBack[1], upRightBack[2]);

    // Ceiling
    glVertex3f(upLeftBack[0], upLeftBack[1], upLeftBack[2]);
    glVertex3f(upLeftFront[0], upLeftFront[1], upLeftFront[2]);
    glVertex3f(upRightFront[0], upRightFront[1], upRightFront[2]);
    glVertex3f(upRightBack[0], upRightBack[1], upRightBack[2]);

    // Floor
    glVertex3f(bottomLeftFront[0], bottomLeftFront[1], bottomLeftFront[2]);
    glVertex3f(bottomLeftBack[0], bottomLeftBack[1], bottomLeftBack[2]);
    glVertex3f(bottomRightBack[0], bottomRightBack[1], bottomRightBack[2]);
    glVertex3f(bottomRightFront[0], bottomRightFront[1], bottomRightFront[2]);

    glEnd();
}

/**
 * fonction pour générer et dessiner la grille
 */
void generateGrid(std::vector<Vec3> &positions, std::vector<Vec3> &normals, int gridSize, float step, float minVal, float maxVal) {
    positions.resize(gridSize * gridSize * gridSize);
    normals.resize(positions.size());
    unsigned int index = 0;
    for (int x = 0; x < gridSize; ++x) {
        for (int y = 0; y < gridSize; ++y) {
            for (int z = 0; z < gridSize; ++z) {
                positions[index] = Vec3(
                        minVal + x * step,
                        minVal + y * step,
                        minVal + z * step
                );
                index++;
            }
        }
    }

    //test affichage voxels
    /*for (int x = 0; x < gridSize; x++) {
        for (int y = 0; y < gridSize; y++) {
            for (int z = 0; z < gridSize; z++) {
                //if((x == 0 || y == 0 || z == 0 || x == gridSize-1 || y == gridSize-1)){
                    DisplayVoxel(Vec3(minVal + x * step,minVal + y * step,minVal + z * step),step,Vec3(float(x) / gridSize, float(y) / gridSize, float(z) / gridSize));
                //}
            }
        }
    }*/
}

// Hash function for Vec3
struct Vec3Hash {
    size_t operator()(const Vec3& v) const {
        return std::hash<float>()(v[0]) ^ std::hash<float>()(v[1]) ^ std::hash<float>()(v[2]);
    }
};

// Equality comparison for Vec3
struct Vec3Equal {
    bool operator()(const Vec3& a, const Vec3& b) const {
        return a[0] == b[0] && a[1] == b[1] && a[2] == b[2];
    }
};

// Convert vector of Vec3 to unordered_set for faster search
std::unordered_set<Vec3, Vec3Hash, Vec3Equal> convertToSet(const std::vector<Vec3>& vec) {
    return std::unordered_set<Vec3, Vec3Hash, Vec3Equal>(vec.begin(), vec.end());
}

// Check if a voxel exists in the grid
bool voxelExists(const Vec3& voxel, const std::unordered_set<Vec3, Vec3Hash, Vec3Equal>& voxelSet, int gridSize) {
    return voxelSet.find(voxel) != voxelSet.end();
}

// Dilatation
void dilatationVoxel(std::vector<Vec3>& voxelCenters, int gridSize, float step, const Vec3& minVal) {
    std::unordered_set<Vec3, Vec3Hash, Vec3Equal> voxelSet = convertToSet(voxelCenters);
    std::vector<Vec3> newVoxels;

    for (const Vec3& voxel : voxelCenters) {
        for (int x = -1; x <= 1; x++) {
            for (int y = -1; y <= 1; y++) {
                for (int z = -1; z <= 1; z++) {
                    Vec3 neighbor = voxel + Vec3(x * step, y * step, z * step);
                    if (!voxelExists(neighbor, voxelSet, gridSize) &&
                        neighbor[0] >= minVal[0] && neighbor[0] <= minVal[0] + gridSize * step &&
                        neighbor[1] >= minVal[1] && neighbor[1] <= minVal[1] + gridSize * step &&
                        neighbor[2] >= minVal[2] && neighbor[2] <= minVal[2] + gridSize * step) {
                        newVoxels.push_back(neighbor);
                    }
                }
            }
        }
    }

    voxelCenters.insert(voxelCenters.end(), newVoxels.begin(), newVoxels.end());
}

// Erosion
void erosionVoxel(std::vector<Vec3>& voxelCenters, int gridSize, float step, const Vec3& minVal, int seuilErosion) {
    std::unordered_set<Vec3, Vec3Hash, Vec3Equal> voxelSet = convertToSet(voxelCenters);
    std::vector<Vec3> remainingVoxels;

    for (const Vec3& voxel : voxelCenters) {
        int voisinsInactifs = 0;
        for (int x = -1; x <= 1; x++) {
            for (int y = -1; y <= 1; y++) {
                for (int z = -1; z <= 1; z++) {
                    if (x == 0 && y == 0 && z == 0) continue;
                    Vec3 neighbor = voxel + Vec3(x * step, y * step, z * step);
                    if (!voxelExists(neighbor, voxelSet, gridSize)) {
                        voisinsInactifs++;
                    }
                }
            }
        }

        if (voisinsInactifs <= seuilErosion) {
            remainingVoxels.push_back(voxel);
        }
    }

    voxelCenters = remainingVoxels;
}


// fermeture (dilatation + erosion)
void fermetureVoxel(std::vector<Vec3>& voxelCenters, int gridSize, float step, const Vec3& minVal, int nbIter) {
    std::cout << "Début fermeture" << std::endl;
    for(int iter = 0; iter < nbIter; iter++){
        std::cout << "Dilatation n°" << iter+1 << std::endl;
        dilatationVoxel(voxelCenters, gridSize, step, minVal);
    }

    for(int iter = 0; iter < nbIter; iter++){
        std::cout << "Erosion n°" << iter+1 << std::endl;
        erosionVoxel(voxelCenters, gridSize, step, minVal, 2);
    }
    std::cout << "Fin fermeture" << std::endl;
}

void ajoutTrou(std::vector<Vec3>& voxelCenters) {
    std::vector<Vec3> updatedVoxelCenters;
    for (const Vec3& voxel : voxelCenters) {
        if ((double)rand() / RAND_MAX >= 0.125) {
            updatedVoxelCenters.push_back(voxel);
        }
    }
    std::cout << voxelCenters.size() << std::endl;
    voxelCenters = updatedVoxelCenters;
    std::cout << voxelCenters.size() << std::endl;
}



// vérification de la présence d'un sommet point dans un voxel donné
bool isPointInVoxel(const Vec3& point, const Vec3& voxelCenter, float voxelSize) {
    float halfSize = voxelSize * 0.5f;
    return (point[0] >= voxelCenter[0] - halfSize && point[0] <= voxelCenter[0] + halfSize) &&
           (point[1] >= voxelCenter[1] - halfSize && point[1] <= voxelCenter[1] + halfSize) &&
           (point[2] >= voxelCenter[2] - halfSize && point[2] <= voxelCenter[2] + halfSize);
}

// Si un sommet ou le  barycentre d'un triangle du mesh se trouve dans un voxel, alors ce voxel est ajouté dans voxelCenters pour être dessiné plus tard
void voxeliseMesh(const Mesh& mesh, const std::vector<Vec3>& gridPositions, float voxelSize, std::vector<Vec3>& voxelCenters) {
    std::unordered_set<Vec3, Vec3Hash, Vec3Equal> voxelSet = convertToSet(gridPositions);

    for (const Triangle& triangle : mesh.triangles) {
        Vec3 barycentre = (mesh.vertices[triangle[0]] + mesh.vertices[triangle[1]] + mesh.vertices[triangle[2]]) / 3.0f;

        for (const Vec3& gridPos : gridPositions) {
            if (isPointInVoxel(barycentre, gridPos, voxelSize)) {
                voxelCenters.push_back(gridPos);
            }
        }

        for (int i = 0; i < 3; ++i) {
            const Vec3& vertex = mesh.vertices[triangle[i]];
            if (voxelSet.find(vertex) != voxelSet.end()) {
                voxelCenters.push_back(vertex);
            }
        }
    }

    // suppression des doublons
    std::sort(voxelCenters.begin(), voxelCenters.end(), [](const Vec3& a, const Vec3& b) {
        return (a[0] < b[0]) || (a[0] == b[0] && a[1] < b[1]) || (a[0] == b[0] && a[1] == b[1] && a[2] < b[2]);
    });
    voxelCenters.erase(std::unique(voxelCenters.begin(), voxelCenters.end()), voxelCenters.end());
}

// affichage de la couleur de chaque voxel en fonction de sa position, afin de délimiter visuellement chaque voxel
Vec3 calculateVoxelColor(const Vec3& position, const Vec3& gridMin, const Vec3& gridMax) {
    float xNormalized = (position[0] - gridMin[0]) / (gridMax[0] - gridMin[0]);
    float yNormalized = (position[1] - gridMin[1]) / (gridMax[1] - gridMin[1]);
    float zNormalized = (position[2] - gridMin[2]) / (gridMax[2] - gridMin[2]);
    return Vec3(xNormalized, yNormalized, zNormalized);
}

// dessiner le mesh voxelisé
void drawVoxels(const std::vector<Vec3>& voxelCenters, float voxelSize, const Vec3& gridMin, const Vec3& gridMax) {
    for (const Vec3& center : voxelCenters) {
        Vec3 color = calculateVoxelColor(center, gridMin, gridMax);
        DisplayVoxel(center, voxelSize, color);
    }
}

void simplifyMesh(Mesh& mesh, float l, int nbIters) {
    for (int iter = 0; iter < nbIters; iter++) {
        // Copie des arêtes pour éviter de modifier la liste pendant l'itération
        std::vector<Edge> edgesCopy = mesh.edges;

        // Fractionnement des arêtes
        for (Edge& e : edgesCopy) {
            float edgeLength = longueurEdge(mesh.vertices[e.debut], mesh.vertices[e.fin]);
            if (edgeLength > (4.0/3.0) * l) {
                splitEdge(e, mesh);
            }
        }

        // Mise à jour des arêtes après le fractionnement
        mesh.edges.clear();
        for (const Triangle& t : mesh.triangles) {
            mesh.ajouterEdgeSiUnique(Edge(t[0], t[1]));
            mesh.ajouterEdgeSiUnique(Edge(t[1], t[2]));
            mesh.ajouterEdgeSiUnique(Edge(t[2], t[0]));
        }

        // Copie des arêtes pour éviter de modifier la liste pendant l'itération
        std::vector<Edge> edgesCopy2 = mesh.edges;

        // Réduction des arêtes
        for (Edge& e : edgesCopy2) {
            float edgeLength = longueurEdge(mesh.vertices[e.debut], mesh.vertices[e.fin]);
            if (edgeLength < (4.0/5.0) * l) {
                collapseEdge(e, mesh);
            }
        }

        // Mise à jour des arêtes après la réduction
        mesh.edges.clear();
        for (const Triangle& t : mesh.triangles) {
            mesh.ajouterEdgeSiUnique(Edge(t[0], t[1]));
            mesh.ajouterEdgeSiUnique(Edge(t[1], t[2]));
            mesh.ajouterEdgeSiUnique(Edge(t[2], t[0]));
        }

        // Elimination des "triangles à deux côtés"
        elimineTADCs(mesh);

        // Smooth
        taubinSmooth(0.330,-0.331,10);
        mesh.computeNormals();
    }
    voxelCenters.clear();
    drawOnce = true; //calcul des voxels
}

double rmse(const std::vector<Vec3>& voxelsCenters1, const std::vector<Vec3>& voxelsCenters2, int gridSize, float step, const Vec3& minVal) {
    std::unordered_set<Vec3, Vec3Hash, Vec3Equal> voxelSet1 = convertToSet(voxelsCenters1);
    std::unordered_set<Vec3, Vec3Hash, Vec3Equal> voxelSet2 = convertToSet(voxelsCenters2);

    double error = 0.0;
    int count = 0;

    for (int x = 0; x < gridSize; ++x) {
        for (int y = 0; y < gridSize; ++y) {
            for (int z = 0; z < gridSize; ++z) {
                Vec3 voxelPos = Vec3(minVal[0] + x * step, minVal[1] + y * step, minVal[2] + z * step);
                bool exists1 = voxelExists(voxelPos, voxelSet1, gridSize);
                bool exists2 = voxelExists(voxelPos, voxelSet2, gridSize);
                error += (exists1 - exists2) * (exists1 - exists2);
                count++;
            }
        }
    }

    return sqrt(error / count);
}
// Function to compare if two normals are approximately equal
bool areNormalsEqual(const Vec3& n1, const Vec3& n2, float epsilon = 0.001f) {
    // Check if the difference between the normals is within the epsilon threshold
    return (fabs(n1[0] - n2[0]) < epsilon &&
            fabs(n1[1] - n2[1]) < epsilon &&
            fabs(n1[2] - n2[2]) < epsilon);
}

void corrigerDefautsConnectivite(std::vector<Vec3>& voxelCenters, float voxelSize) {
    std::unordered_set<Vec3, Vec3Hash, Vec3Equal> voxelSet;
    for (const Vec3& voxelCenter : voxelCenters) {
        voxelSet.insert(voxelCenter);
    }

    std::cout << "Début correction" << std::endl;
    std::vector<Vec3> nouveauxVoxelCenters;

    // Iterate through each voxel center
    for (const Vec3& center : voxelCenters) {
        bool isVoxelDefective = false;

        // Check connectivity in all six directions around the current voxel
        for (int i = -1; i <= 1; ++i) {
            for (int j = -1; j <= 1; ++j) {
                for (int k = -1; k <= 1; ++k) {
                    // Skip the center voxel itself
                    if (i == 0 && j == 0 && k == 0) {
                        continue;
                    }

                    Vec3 neighbor(center[0] + i * voxelSize, center[1] + j * voxelSize, center[2] + k * voxelSize);
                    Vec3 opposite(center[0] - i * voxelSize, center[1] - j * voxelSize, center[2] - k * voxelSize);

                    // Check if the neighboring voxel exists in the set
                    if (voxelSet.find(neighbor) == voxelSet.end() && voxelSet.find(opposite) != voxelSet.end()) {
                        isVoxelDefective = true;
                        break;
                    }
                }
                if (isVoxelDefective) {
                    break;
                }
            }
            if (isVoxelDefective) {
                break;
            }
        }

        // If the voxel is defective, add its neighbors to correct connectivity
        if (isVoxelDefective) {
            for (int i = -1; i <= 1; ++i) {
                for (int j = -1; j <= 1; ++j) {
                    for (int k = -1; k <= 1; ++k) {
                        if (i == 0 && j == 0 && k == 0) {
                            continue;
                        }
                        Vec3 neighbor(center[0] + i * voxelSize, center[1] + j * voxelSize, center[2] + k * voxelSize);
                        Vec3 opposite(center[0] - i * voxelSize, center[1] - j * voxelSize, center[2] - k * voxelSize);

                        // Check if the neighboring voxel exists in the set
                        if (voxelSet.find(neighbor) == voxelSet.end() && voxelSet.find(opposite) != voxelSet.end()) {
                            nouveauxVoxelCenters.push_back(neighbor);
                        }
                    }
                }
            }
        }
    }

    // Add the original voxelCenters and the newly corrected voxels
    voxelCenters.insert(voxelCenters.end(), nouveauxVoxelCenters.begin(), nouveauxVoxelCenters.end());

    std::cout << "Fin correction" << std::endl;
}


void exportVoxelsToOFF(const std::vector<Vec3>& voxelCenters, float voxelSize) {
    std::ofstream outfile("voxels.off");
    if (!outfile.is_open()) {
        std::cerr << "Unable to open output file!" << std::endl;
        return;
    }

    // Header
    outfile << "OFF\n";
    outfile << voxelCenters.size() << " 0 0\n";

    // Write voxel vertices
    for (const auto& voxel : voxelCenters) {
        outfile << voxel[0] << " " << voxel[1] << " " << voxel[2] << "\n";
    }

    // Add comments specifying voxel size
    outfile << "# Voxel size: " << voxelSize << "\n";

    outfile.close();
}
void initButtons() {
    buttons.push_back(Button{Vec3(0, 15, 0), 75, 20, "Simplification", []() {
        simplifyMesh(mesh, 0.05, 5);
    }});

    buttons.push_back(Button{Vec3(0, 40, 0), 75, 20, "RMSE", []() {
        Mesh meshCopy = mesh;
        std::vector<Vec3> originalVoxelCenters = voxelCenters;
        simplifyMesh(meshCopy, 0.05, 5);

        std::vector<Vec3> voxelCentersCopy;
        voxeliseMesh(meshCopy, positionsGrille, step, voxelCentersCopy);
        std::vector<Vec3> voxelCentersOriginal = voxelCenters;

        rmseApresSimplification = rmse(voxelCentersOriginal, voxelCentersCopy, gridSize, step, gridMin);

        std::vector<Vec3> voxelCentersFermeture = originalVoxelCenters;
        ajoutTrou(voxelCentersFermeture);
        fermetureVoxel(voxelCentersFermeture, gridSize, step, gridMin,3);

        rmseApresFermeture = rmse(originalVoxelCenters, voxelCentersFermeture, gridSize, step, gridMin);

        std::vector<Vec3> voxelCentersConnect = voxelCenters;
        ajoutTrou(voxelCentersConnect);
        corrigerDefautsConnectivite(voxelCenters,step);

        rmseApresConnect = rmse(originalVoxelCenters, voxelCentersConnect, gridSize, step, gridMin);
    }});

    buttons.push_back(Button{Vec3(0, 65, 0), 75, 20, "Fermeture (3 iters)", []() {
        fermetureVoxel(voxelCenters, gridSize, step, gridMin, 3);
    }});

    buttons.push_back(Button{Vec3(0, 90, 0), 75, 20, "Erosion (Seuil = 2)", []() {
        erosionVoxel(voxelCenters, gridSize, step, gridMin, 2);
    }});

    buttons.push_back(Button{Vec3(0, 115, 0), 75, 20, "Dilatation", []() {
        dilatationVoxel(voxelCenters, gridSize, step, gridMin);
    }});

    buttons.push_back(Button{Vec3(0, 140, 0), 75, 20, "Connectivite", []() {
        std::vector<Vec3> voxelCentersConnect = voxelCenters;
        corrigerDefautsConnectivite(voxelCenters,step);
    }});

    buttons.push_back(Button{Vec3(0, 165, 0), 75, 20, "Trou", []() {
        ajoutTrou(voxelCenters);
    }});

    buttons.push_back(Button{Vec3(0, 190, 0), 75, 20, "Export", []() {
        exportVoxelsToOFF(voxelCenters,step);
    }});
}

Vec3 screenToOrtho(int x, int y) {
    return Vec3(x, SCREENHEIGHT - y, 0);
}

// Gestion des clics de souris
void mouseButtonCallback(int button, int state, int x, int y) {
    if (button == GLUT_LEFT_BUTTON && state == GLUT_DOWN) {
        Vec3 orthoCoords = screenToOrtho(x, y);
        for (const auto& btn : buttons) {
            if (orthoCoords[0] >= btn.position[0] - btn.width / 2 && orthoCoords[0] <= btn.position[0] + btn.width / 2 &&
                orthoCoords[1] >= btn.position[1] - btn.height / 2 && orthoCoords[1] <= btn.position[1] + btn.height / 2) {
                std::cout << "click sur bouton" << std::endl;
                btn.onClick();
                break;
            }
        }
    }
}


/**
 * projection orthographique, boutons toujours affichés en 2d par-dessus le rendu 3d
 */
void draw2DOverlay() {
    glMatrixMode(GL_PROJECTION);
    glPushMatrix();
    glLoadIdentity();
    gluOrtho2D(0, SCREENWIDTH, 0, SCREENHEIGHT);
    glMatrixMode(GL_MODELVIEW);
    glPushMatrix();
    glLoadIdentity();

    drawText("MeshVoxStudio", 750, 850);

    for (const auto& button : buttons) {
        drawButton(button);
    }

    if (rmseApresSimplification >= 0.0 && rmseApresFermeture >= 0.0 && rmseApresConnect >=0.0) {
        std::string result1 = "RMSE apres simplification: " + std::to_string(rmseApresSimplification);
        std::string result2 = "RMSE apres fermeture: " + std::to_string(rmseApresFermeture);
        std::string result3 = "RMSE apres connect: " + std::to_string(rmseApresConnect);
        drawText(result1, 350, 35);
        drawText(result2, 90, 35);
        drawText(result3, 650, 35);
    }

    glPopMatrix();
    glMatrixMode(GL_PROJECTION);
    glPopMatrix();
    glMatrixMode(GL_MODELVIEW);
}

//Draw fonction
void draw() {

    if (displayMode == LIGHTED || displayMode == LIGHTED_WIRE) {

        glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
        glEnable(GL_LIGHTING);

    } else if (displayMode == WIRE) {

        glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
        glDisable(GL_LIGHTING);

    } else if (displayMode == SOLID) {
        glDisable(GL_LIGHTING);
        glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

    }

    glColor3f(0.8, 1, 0.8);
    //drawMesh(mesh, true);

    if (displayMode == SOLID || displayMode == LIGHTED_WIRE) {
        glEnable(GL_POLYGON_OFFSET_LINE);
        glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
        glLineWidth(1.0f);
        glPolygonOffset(-2.0, 1.0);

        glColor3f(0., 0., 0.);
        drawMesh(mesh, false);

        glDisable(GL_POLYGON_OFFSET_LINE);
        glEnable(GL_LIGHTING);
    }

    glDisable(GL_LIGHTING);
    if (display_normals) {
        glColor3f(1., 0., 0.);
        drawNormals(mesh);
    }

    glEnable(GL_LIGHTING);

    //dessiner la grille
    generateGrid(positionsGrille,normalsGrille, gridSize, step, minVal, maxVal);
    glPointSize(2);
    glColor3f(1, 0, 1);
    //drawPointSet(positionsGrille, normalsGrille);

    //voxelisaton
    if(drawOnce){
        voxeliseMesh(mesh, positionsGrille, step, voxelCenters);
        drawOnce = false;
    }
    drawVoxels(voxelCenters, step, gridMin, gridMax);

    //boutons
    draw2DOverlay();

}

void changeDisplayMode() {
    if (displayMode == LIGHTED)
        displayMode = LIGHTED_WIRE;
    else if (displayMode == LIGHTED_WIRE)
        displayMode = SOLID;
    else if (displayMode == SOLID)
        displayMode = WIRE;
    else
        displayMode = LIGHTED;
}

void display() {
    glLoadIdentity();
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    camera.apply();
    draw();
    glFlush();
    glutSwapBuffers();
}

void idle() {
    glutPostRedisplay();
}



// ------------------------------------
// User inputs
// ------------------------------------
//Keyboard event
void key(unsigned char keyPressed, int x, int y) {
    switch (keyPressed) {
        case 'f':
            if (fullScreen == true) {
                glutReshapeWindow(SCREENWIDTH, SCREENHEIGHT);
                fullScreen = false;
            } else {
                glutFullScreen();
                fullScreen = true;
            }
            break;

        case 'w':
            changeDisplayMode();
            break;

        case 'n': //Press n key to display normals
            display_normals = !display_normals;
            break;

        case '1': //Toggle loaded mesh display
            display_mesh = !display_mesh;
            break;

        case '2':
            simplifyMesh(mesh,0.05,5); //5 iters recommandées dans papier
            break;

        case '3':
            fermetureVoxel(voxelCenters,gridSize,step,gridMin,3);
            break;

        case '4': {
            Mesh meshCopy = mesh;
            std::vector<Vec3> originalVoxelCenters = voxelCenters;
            simplifyMesh(meshCopy, 0.05, 5);

            std::vector<Vec3> voxelCentersCopy;
            voxeliseMesh(meshCopy, positionsGrille, step, voxelCentersCopy);
            std::vector<Vec3> voxelCentersOriginal = voxelCenters;

            //rmse entre le mesh originel et le mesh simplifié
            double r1 = rmse(voxelCentersOriginal, voxelCentersCopy, gridSize, step, gridMin);

            std::vector<Vec3> voxelCentersFermeture = originalVoxelCenters;
            ajoutTrou(voxelCentersFermeture); // ajout de trous dans la copie du mesh voxélisée
            fermetureVoxel(voxelCentersFermeture, gridSize, step, gridMin,3);

            //rmse entre le mesh originel et le mesh corrigé avec fermeture
            double r2 = rmse(originalVoxelCenters, voxelCentersFermeture, gridSize, step, gridMin);

            // Afficher les résultats
            std::cout << "Résolution de la grille: " << gridSize << "x" << gridSize << "x" << gridSize << std::endl;
            std::cout << "RMSE après simplification: " << r1 << std::endl;
            std::cout << "RMSE après fermeture: " << r2 << std::endl;

            break;
        }

        case '5':
            ajoutTrou(voxelCenters);
            break;

        case '7':
            dilatationVoxel(voxelCenters, gridSize, step, gridMin);
            break;

        case '8':
            erosionVoxel(voxelCenters, gridSize, step, gridMin, 2);
            break;

        case 's': //Switches between face normals and vertices normals
            display_smooth_normals = !display_smooth_normals;
            break;

        case '+':
            gridSize++;
            std::cout << "resolution: " << gridSize << std::endl;
            generateGrid(positionsGrille, normalsGrille, gridSize, (maxVal - minVal) / (gridSize - 1), minVal, maxVal);
            voxeliseMesh(mesh, positionsGrille, (maxVal - minVal) / (gridSize - 1), voxelCenters);
            break;

        case '-':
            if (gridSize > 4) {
                gridSize--;
                std::cout << "resolution: " << gridSize << std::endl;
                generateGrid(positionsGrille, normalsGrille, gridSize, (maxVal - minVal) / (gridSize - 1), minVal, maxVal);
                voxeliseMesh(mesh, positionsGrille, (maxVal - minVal) / (gridSize - 1), voxelCenters);
            }
            break;

        case 'q':
            glutDestroyWindow (window);
            exit (0);
            break;
        case 27: //ECHAP
            glutDestroyWindow (window);
            exit (0);
            break;
        default:
            break;
    }
    idle();
}

//Mouse events
void mouse(int button, int state, int x, int y) {
    mouseButtonCallback(button, state, x, y); //gérer les clics sur les boutons

    if (state == GLUT_UP) {
        mouseMovePressed = false;
        mouseRotatePressed = false;
        mouseZoomPressed = false;
    } else {
        if (button == GLUT_LEFT_BUTTON) {
            camera.beginRotate(x, y);
            mouseMovePressed = false;
            mouseRotatePressed = true;
            mouseZoomPressed = false;
        } else if (button == GLUT_RIGHT_BUTTON) {
            lastX = x;
            lastY = y;
            mouseMovePressed = true;
            mouseRotatePressed = false;
            mouseZoomPressed = false;
        } else if (button == GLUT_MIDDLE_BUTTON) {
            if (mouseZoomPressed == false) {
                lastZoom = y;
                mouseMovePressed = false;
                mouseRotatePressed = false;
                mouseZoomPressed = true;
            }
        }
    }

    idle();
}

//Mouse motion, update camera
void motion(int x, int y) {
    if (mouseRotatePressed == true) {
        camera.rotate(x, y);
    } else if (mouseMovePressed == true) {
        camera.move((x - lastX) / static_cast<float>(SCREENWIDTH), (lastY - y) / static_cast<float>(SCREENHEIGHT), 0.0);
        lastX = x;
        lastY = y;
    } else if (mouseZoomPressed == true) {
        camera.zoom(float(y - lastZoom) / SCREENHEIGHT);
        lastZoom = y;
    }
}


void reshape(int w, int h) {
    camera.resize(w, h);
}

// ------------------------------------
// Start of graphical application
// ------------------------------------
int main(int argc, char **argv) {
    if (argc > 2) {
        exit(EXIT_FAILURE);
    }
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_RGBA | GLUT_DEPTH | GLUT_DOUBLE);
    glutInitWindowSize(SCREENWIDTH, SCREENHEIGHT);
    window = glutCreateWindow("MeshVoxStudio");

    init();
    glutIdleFunc(idle);
    glutDisplayFunc(display);
    glutKeyboardFunc(key);
    glutReshapeFunc(reshape);
    glutMotionFunc(motion);
    glutMouseFunc(mouse);
    key('?', 0, 0);

    //Mesh loaded with precomputed normals
    openOFF("data/avion_n.off", mesh.vertices, mesh.normals, mesh.triangles, mesh.triangle_normals);

    mesh.computeNormals();
    collect_one_ring(mesh.vertices, mesh.triangles, mesh.oneRing);

    current_field.clear();

    initButtons();

    glutMainLoop();
    return EXIT_SUCCESS;
}